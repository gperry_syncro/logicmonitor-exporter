package main

import (
	//standard libs
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	//local libs

	//remote libs
	"golang.org/x/sync/errgroup"
)

//func name, return type (return a function that returns an error)
func pinger(ctx context.Context) func() error {
	return func() error {
		select {
		case <-ctx.Done():
			println("little context is done")
			return nil
		}
		return fmt.Errorf("boom")
	}
}

//go mod init go-modules.syncromatics.com/logicmonitor-exporter

func main() {
	// http.HandleFunc("/", handler)
	// log.Fatal(http.ListenAndServe(":8080", nil))

	fmt.Println("starting the app!")

	//create little context that can be cancelled
	ctx, cancel := context.WithCancel(context.Background())

	//create an error group in the little context that can react to signals received on a channel
	group, ctx := errgroup.WithContext(ctx)

	//start your go routine, which immediately returns an error to the little context
	group.Go(pinger(ctx))

	//create channel to send/recieve os.signals
	eventChan := make(chan os.Signal)

	//tel os.signal i want to be notified on channel "eventChan" if sigint or sigterm happens
	signal.Notify(eventChan, syscall.SIGINT, syscall.SIGTERM)

	//wait for a signal
	select {
	case s := <-eventChan:
		fmt.Printf("received signal %s \n", s)
	case <-ctx.Done():
		fmt.Println("problemo")
	}

	cancel()

	if err := group.Wait(); err != nil {
		panic(err)
	}
}
