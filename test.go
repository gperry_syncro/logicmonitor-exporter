// //declare a channel
// var out chan map[int]int

// func pinge1r(out chan<- map[int]int) {
// 	// initialize a map
// 	m := make(map[int]int)
// 	for {

// 		resp, err := http.Get("https://www.yahoo.com")
// 		if err != nil {
// 			panic(err)
// 		}

// 		// add/increment status code map and send to channel waiting in parent function
// 		m[resp.StatusCode] = m[resp.StatusCode] + 1

// 		out <- m

// 		//sleep for 5 seconds
// 		time.Sleep(time.Second * 5)
// 	}
// }

// func handler(w http.ResponseWriter, r *http.Request) {

// 	// if we are fresh, init the channel and send into goroutine
// 	if len(out) == 0 {
// 		out := make(chan map[int]int)
// 		go pinger(out)
// 	}

// 	m1 := <-out

// 	data, err := json.Marshal(m1)
// 	if err != nil {
// 		panic(err)
// 	}

// 	w.Write(data)
// }